﻿#include "pong.h"
#include <graphics.h>
#include <conio.h>
#include <time.h>
#include <string>
#include <Windows.h>

bool isGameRunning = true;		// 游戏是否还在运行当中
Ball ball;						// 小球
Paddle left_paddle;				// 左边挡板
Paddle right_paddle;			// 右边挡板
int left_score = 0;				// 左边得分
int right_score = 0;			// 右边得分

// 初始化游戏
void InitGame()
{
	initgraph(WINWIDTH, WINHEIGHT);
	setbkcolor(WHITE);
	srand((unsigned)time(NULL));
	HWND hWnd = GetHWnd();
	SetWindowText(hWnd, _T("Pong"));
}

// 初始化小球的坐标和速度
void InitBall()
{
	ball.w = BALLRADIUS * 2;
	ball.h = BALLRADIUS * 2;
	ball.x = WINWIDTH / 2 - ball.w / 2;
	ball.y = WINHEIGHT / 2 - ball.h / 2;

	double rnd = rand() / (RAND_MAX + 1.0);
	if (rnd < 0.5)
	{
		ball.speedX = 2;
	}
	else
	{
		ball.speedX = -2;
	}

	rnd = rand() / (RAND_MAX + 1.0);
	if (rnd < 0.5)
	{
		ball.speedY = 2;
	}
	else
	{
		ball.speedY = -2;
	}
}

// 初始化挡板的坐标的速度
void InitPaddles()
{
	left_paddle.x = 40;
	left_paddle.y = WINHEIGHT / 2 - PADDLEHEIGHT / 2;
	left_paddle.w = PADDLEWIDTH;
	left_paddle.h = PADDLEHEIGHT;
	left_paddle.speed = 5;

	right_paddle.x = WINWIDTH - (40 + PADDLEWIDTH);
	right_paddle.y = WINHEIGHT / 2 - PADDLEHEIGHT / 2;
	right_paddle.w = PADDLEWIDTH;
	right_paddle.h = PADDLEHEIGHT;
	right_paddle.speed = 5;
}

// 退出游戏
void QuitGame()
{
	closegraph();
	exit(0);
}

// 游戏主循环函数
void GameLoop()
{
	DWORD currFrameTime;
	DWORD nextFrameTime;
	DWORD delayFrameTime;

	while (isGameRunning)
	{
		InitBall();
		InitPaddles();

		while (isGameRunning)
		{
			currFrameTime = GetTickCount();

			// 判断是否有键盘按键被按下
			if (_kbhit())
			{
				int k = _getch();
				KeyboardControl(k);
			}

			MoveBall();

			BeginBatchDraw();
			DrawView();
			EndBatchDraw();
			
			nextFrameTime = currFrameTime + 1000 / 30;
			delayFrameTime = nextFrameTime - GetTickCount();
			
			if (delayFrameTime > 0)
			{
				Sleep(delayFrameTime);
			}
		}

		RestartGame();
	}
}

// 按键处理
void KeyboardControl(int key)
{
	switch (key)
	{
	case 108:	// 'l'
		MoveRightPaddleDown();
		break;
	case 111:	// 'o'
		MoveRightPaddleUp();
		break;
	case 113:	// 'q'
		isGameRunning = false;
		break;
	case 115:	// 's'
		MoveLeftPaddleDown();
		break;
	case 119:	// 'w'
		MoveLeftPaddleUp();
		break;
	default:
		break;
	}
}

// 绘制游戏视图元素
void DrawView()
{
	cleardevice();
	DrawScores();
	DrawBall();
	DrawPaddles();
}

// 绘制得分
void DrawScores()
{

}

// 绘制小球
void DrawBall()
{
	setfillcolor(RED);
	solidcircle(ball.x, ball.y, BALLRADIUS);
}

// 绘制挡板
void DrawPaddles()
{
	setfillcolor(GREEN);
	solidrectangle(left_paddle.x, left_paddle.y, left_paddle.x + PADDLEWIDTH, left_paddle.y + PADDLEHEIGHT);
	solidrectangle(right_paddle.x, right_paddle.y, right_paddle.x + PADDLEWIDTH, right_paddle.y + PADDLEHEIGHT);
}

// 移动小球
void MoveBall()
{
	ball.x += ball.speedX;
	ball.y += ball.speedY;

	if (collision(ball, left_paddle) || collision(ball, right_paddle))
	{
		ball.speedX *= -1;
	}

	if (ball.y < 0 || ball.y + ball.h > WINHEIGHT)
	{
		ball.speedY *= -1;
	}

	// 小球移出左边界
	if (ball.x < 0)
	{
		++right_score;
		InitBall();
		InitPaddles();
	}

	// 小球移出右边界
	if (ball.x + ball.w > WINWIDTH)
	{
		++left_score;
		InitBall();
		InitPaddles();
	}
}

// 碰撞检测
bool collision(Ball ball, Paddle paddle)
{
	int ball_left, paddle_left;
	int ball_right, paddle_right;
	int ball_top, paddle_top;
	int ball_bottom, paddle_bottom;

	ball_left = ball.x;
	ball_right = ball.x + ball.w;
	ball_top = ball.y;
	ball_bottom = ball.y + ball.h;

	paddle_left = paddle.x;
	paddle_right = paddle.x + paddle.w;
	paddle_top = paddle.y;
	paddle_bottom = paddle.y + paddle.h;

	if (ball_left > paddle_right)
	{
		return false;
	}

	if (ball_right < paddle_left)
	{
		return false;
	}

	if (ball_top > paddle_bottom)
	{
		return false;
	}

	if (ball_bottom < paddle_top)
	{
		return false;
	}

	return true;
}

// 向上移动左边挡板
void MoveLeftPaddleUp()
{
	left_paddle.y -= left_paddle.speed;
	if (left_paddle.y < 10)
	{
		left_paddle.y = 10;
	}
}

// 向下移动左边挡板
void MoveLeftPaddleDown()
{
	left_paddle.y += left_paddle.speed;
	if (left_paddle.y > WINHEIGHT - PADDLEHEIGHT)
	{
		left_paddle.y = WINHEIGHT - PADDLEHEIGHT;
	}
}

// 向上移动右边挡板
void MoveRightPaddleUp()
{
	right_paddle.y -= right_paddle.speed;
	if (right_paddle.y < 10)
	{
		right_paddle.y = 10;
	}
}

// 向下移动右边挡板
void MoveRightPaddleDown()
{
	right_paddle.y += right_paddle.speed;
	if (right_paddle.y > WINHEIGHT - PADDLEHEIGHT)
	{
		right_paddle.y = WINHEIGHT - PADDLEHEIGHT;
	}
}

// 重新开始游戏
void RestartGame()
{
	cleardevice();

	int startX = 220;
	int startY = 100;

	char s[20];

	setlinecolor(GREEN);
	rectangle(50, 50, 590, 400);

	settextcolor(GREEN);
	settextstyle(50, 0, _T("宋体"));
	outtextxy(startX, startY, _T("游戏结束"));

	settextstyle(30, 0, _T("宋体"));
	sprintf_s(s, "左边得分：%d", left_score);
	outtextxy(startX - 120, startY + 100, _T(s));

	settextstyle(30, 0, _T("宋体"));
	sprintf_s(s, "右边得分：%d", right_score);
	outtextxy(startX + 120, startY + 100, _T(s));

	settextstyle(30, 0, _T("宋体"));
	outtextxy(startX - 10, startY + 200, _T("按r键重新开始"));

	settextstyle(30, 0, _T("宋体"));
	outtextxy(startX - 10, startY + 250, _T("按q键退出游戏"));

	while (true)
	{
		char k = _getch();

		if (k == 'r')
		{
			isGameRunning = true;
			break;
		}

		if (k == 'q')
		{
			isGameRunning = false;
			break;
		}
	}
}
