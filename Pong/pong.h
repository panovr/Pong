﻿#pragma once

#define WINWIDTH 640
#define WINHEIGHT 480
#define BALLRADIUS 5
#define PADDLEWIDTH 10
#define PADDLEHEIGHT 60
#define TIMESPAN 200

// 小球结构体类型
struct Ball
{
	int x;
	int y;
	int w;
	int h;
	int speedX;
	int speedY;
};

// 挡板结构体类型
struct Paddle
{
	int x;
	int y;
	int w;
	int h;
	int speed;
};

// 绘制得分
void DrawScores();

// 绘制小球
void DrawBall();

// 移动小球
void MoveBall();

// 绘制挡板
void DrawPaddles();

// 初始化游戏
void InitGame();

// 初始化小球的坐标和速度
void InitBall();

// 初始化挡板的坐标的速度
void InitPaddles();

// 退出游戏
void QuitGame();

// 游戏主循环函数
void GameLoop();

// 按键处理
void KeyboardControl(int key);

// 绘制游戏视图元素
void DrawView();

// 碰撞检测
bool collision(Ball ball, Paddle paddle);

// 向上移动左边挡板
void MoveLeftPaddleUp();

// 向下移动左边挡板
void MoveLeftPaddleDown();

// 向上移动右边挡板
void MoveRightPaddleUp();

// 向下移动右边挡板
void MoveRightPaddleDown();

// 重新开始游戏
void RestartGame();
